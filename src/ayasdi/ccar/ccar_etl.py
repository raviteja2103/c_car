import Quandl
from Quandl import Quandl
import pandas as pd
import numpy as np
import math
import sqlalchemy
import datetime
import logging

import ConfigParser
import pickle

import sqlite3
import sqlalchemy
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from pandas.io import sql


def formatRevenue(self,start_date,end_date):
	"""Only selects dates from Revenue that are within date range"""
	self = self.set_index(pd.DatetimeIndex(self['Date'],inplace=True))
	self = self.drop('Date', 1)
	self_subset = self[(self.index>=start_date)&(self.index<=end_date)]
	return self_subset

def formatData(myd, date_range):
	"""Zero-order hold interpolation and 15th of every month selected"""
	myd.index = myd.index.to_period('D').to_timestamp('D')
	myd = myd.reindex(date_range)
	myd = myd.fillna(method='ffill')
	self_normal = myd[myd.index.day == 15]
	return self_normal

def formatDateColumns(self):
	col_head = self.columns.to_pydatetime()
	col_head_format = np.vectorize(lambda s: s.strftime('%Y-%m'))(col_head)
	self.columns = col_head_format
	return self.T

def buildDerivedFeatures(self):
	"""Builds additional lags, logs, etc features on original data"""
	"""Take Logs first and then lags and delta"""
	new_dict = {}
	for col_name in self.columns.values:
		new_dict[col_name]=self[col_name]
		log_val = np.log(self[col_name])
		square_val = self[col_name]**2

		new_dict['%s_Lag_1' %(col_name)]=self[col_name].shift(1)
		new_dict['%s_Lag_1_delta' %(col_name)]=self[col_name].diff().shift(1)
		new_dict['%s_Lag_1_deltaLog' %(col_name)] = log_val.diff().shift(1)
		new_dict['%s_Lag_1_log' %(col_name)] = log_val.shift(1)
		new_dict['%s_Lag_1_square' %(col_name)] = square_val.shift(1)
 
		new_dict['%s_Lag_2' %(col_name)]=self[col_name].shift(2)
		new_dict['%s_Lag_2_delta' %(col_name)]=self[col_name].diff().shift(2)
		new_dict['%s_Lag_2_deltaLog' %(col_name)] = log_val.diff().shift(2)
		new_dict['%s_Lag_2_log' %(col_name)] = log_val.shift(2)
		new_dict['%s_Lag_2_square' %(col_name)] = square_val.shift(2)

		new_dict['%s_delta' %(col_name)] = self[col_name].diff()
		new_dict['%s_deltaLog' %(col_name)]	= np.log(self[col_name]).diff()
		new_dict['%s_log' %(col_name)] = np.log(self[col_name])
		new_dict['%s_square' %(col_name)] = self[col_name]**2 

	res=pd.DataFrame(new_dict,index=self.index)	
	return res

def calcCorr(self, rev_input):	
	"""Only calculate coefficient on values that are not invalid"""
	cor_list = []
	for col_name in self.columns.values:
		a1 = self[col_name]
		masked_a1 = np.ma.masked_invalid(a1)
		a2 = rev_input
		masked_a2 = np.ma.masked_invalid(a2)
		cor_list.append(np.ma.corrcoef(masked_a1.T,masked_a2.T)[0][1])	
	return np.array(cor_list)	

def print_full(x):
    """Print all rows of Dataframe"""
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')


def sqlite_connect():

	con = None
	try:
		con = lite.connect('c_car.db')
		cur = con.cursor()
		cur.execute('SELECT SQLITE_VERSION()')
		#cur.execute('')
		data = cur.fetchone()
		print "SQLite version: %s" % data                

	except lite.Error, e:
		print "Error %s:" % e.args[0]
		sys.exit(1)

	return con		


def sqlite_update(mydata, start_date, end_date):

	import sqlalchemy

	# #Insert into database
	# lite_engine = sqlalchemy.create_engine('sqlite:///c_car.db')
	# mydata.to_sql('c_car_%s'%(time.strftime("%d/%m/%Y")), lite_engine, index=True)

	logging.info("Connect to C_CAR DB")
	car_db = sqlite3.connect('c_car.db')

	tablename = 'ccar' + '_' + start_date[:4] + '_' + end_date[:4]
	#cur = car_db.cursor()
	#cur.execute('DROP Table if exists %s' %tablename)

	logging.info("Write to a table")
	sql.write_frame(mydata, name=tablename, con=car_db)
	logging.info("Read table to a df")
	mydata_db = sql.read_frame('select * from %s'%tablename, car_db)

	return mydata_db

def sqlite_read(start_date, end_date):
	'''Read data from SQLite db in the form of df
	'''
	logging.info("Connect to C_CAR DB")
	car_db = sqlite3.connect('c_car.db')

	tablename = 'ccar' + '_' + start_date[:4] + '_' + end_date[:4]

	logging.info("Read table to a df")
	mydata_db = sql.read_frame('select * from %s'%tablename, car_db)

	logging.info("Updating indices")
	date_range = pd.date_range(start_date, end_date, freq='D')

	mydata_db.set_index(date_range, inplace=True)


	return mydata_db


def main():


    config = ConfigParser.RawConfigParser()
    config.read('c_car.config')

    #Data inputs form config file
    
    #Data inputs form config file
    #"""Load Inputs"""
    # my_authtoken="jRvWAzR5gCDPbKevFrEd"
	# start_date = "2011-11-15"
	# end_date = "2015-05-15"
	#index_list = ["ODA/IND_NGDPD","FRED/GDP","YAHOO/INDEX_VIX.4","FRED/AUD3MTD156N","CHRIS/ASX_YS1","ODA/BRA_NGDPD",""]
	#White_list = "YAHOO/INDEX_HSI", "CHRIS/CME_RP1","CHRIS/CME_RY1","CURRFX/EURUSD","CHRIS/CME_BY1","CURRFX/USDJPY","[FED/RILSPDEPM03_N_B - FRED/DFF]","SCF/CME_JY1_FW","YAHOO/INDEX_FVX")

    my_authtoken  = config.get('Data', 'token')
    index_list = ["ODA/IND_NGDPD","FRED/GDP","YAHOO/INDEX_VIX.4","FRED/AUD3MTD156N","CHRIS/ASX_YS1","ODA/BRA_NGDPD","FRED/CHNCPIALLAINMEI","ODA/CHN_NGDP_RPCH","RATEINF/CPI_EUR","AUTONOMY/SWAP_05Y_EUR","ODA/IDN_NGDPD","FRED/JPY1MTD156N","ODA/KOR_NGDPD","FRED/DEXMXUS","ODA/RUS_NGDPD","ODA/TWN_NGDPD","FRED/TEDRATE","RATEINF/CPI_GBR","FED/RXI_US_N_B_UK","FRED/INTGSBGBM193N","ODA/GBR_NGDPD","UKONS/LMS_MGSX_M","UMICH/SOC1","OFDP/INDEX_MARKIT_CDXNAHY","OFDP/INDEX_MARKIT_CDXNAIG","OFDP/INDEX_CDXEM","RATEINF/CPI_USA","FRED/DFF","BUNDESBANK/BBK01_WT5511","FED/FL075035223_Q","FRED/DGS10","FED/RILSPDEPM01_N_B","FRED/DTB1YR","FED/RILSPDEPM03_N_B","FRED/DTB3","FRED/DSWP5","FED/RILSPDEPM06_N_B","DOE/RWTC","YAHOO/INDEX_GSPC","FRED/UNRATE","FED/FL073163013_Q","ML/BBBEY"]
    rev_data  = config.get('Data', 'r_raw')    
    start_date = config.get('Data', 's_date')
    end_date  = config.get('Data', 'e_date')
    #index_list  = config.get('Data', 'variables')
    date_range = pd.date_range(start_date, end_date, freq='D')
    rev_raw = pd.read_table(rev_data, header=0, sep='\t')
	# logging.info("Getting data from Quandl")
	# mydata = Quandl.get(index_list,trim_start=start_date, trim_end=end_date, authtoken = my_authtoken)

    """Format Inputs"""
    rev_clean = formatRevenue(rev_raw,start_date,end_date)
    rev_clean = formatDateColumns(rev_clean.T)

    #Reading data from SQLite
    myd = sqlite_read(start_date, end_date)

    logging.info("Transforming data to make it monthly")
    mydata_clean = formatData(myd, date_range)
    
    logging.info("Building Lags and logs")
    mydata_clean_derived = buildDerivedFeatures(mydata_clean)

    logging.info("Calculate Correlation")
    corr_mydata_rev = calcCorr(mydata_clean_derived,rev_clean)
    
    logging.info("Iris Ready File with column formatting: Excluding Correlation")
    iris_ready = formatDateColumns(mydata_clean_derived.T).T

    logging.info('Pickling the iris_ready DataFrame to be used in regress script')
    with open('iris_ready.pickle', 'wb') as handle:
		pickle.dump(iris_ready, handle)

    logging.info("Iris Ready File with column formatting: Including Correlation")
    iris_ready_corr = iris_ready.copy()
    iris_ready_corr['corr'] = corr_mydata_rev
    iris_ready_corr['abscorr'] = abs(corr_mydata_rev)

    iris_ready_corr.to_csv('../../../data/ccar_data_5years.csv', sep=',')

if __name__ == "__main__":
 	main()