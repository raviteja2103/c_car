import pandas as pd
import numpy as np
import math
import datetime
import statsmodels.api as sm
from CCAR_Regress import test_model, reg_full, feature_list

"""Load Forecast Dataframe"""
reg_exrev = reg_full.drop('Revenue', 1)
"""Setting up 1 model Dataframe"""
test_pt = reg_exrev[feature_list]
start_df = min(test_pt.index)
end_df = "2017-12-15"
date_range = pd.date_range(start_df, end_df, freq='D')
date_range = date_range[date_range.day==15]
date_range_format = np.vectorize(lambda s: s.strftime('%Y-%m'))(date_range.to_pydatetime())
test_pt = test_pt.reindex(date_range_format)
"""Prediction on desired range"""
#TODO: Use Median of past 6 months for fill values instead
test_pt = test_pt.fillna(method='ffill')
test_pt_const = sm.add_constant(test_pt)
start_predict = '2016-01'
end_predict = '2017-12'
test_pt_range = test_pt_const.ix[(test_pt_const.index>=start_predict) & (test_pt_const.index<=end_predict)]
test_predict = test_model.ModelFit.predict(test_pt_range)
print test_model.ModelFit.params.to_dict().keys(),'+',test_predict
