# -*- coding: utf-8 -*-
"""
Created on Wed Jan  7 22:05:23 2015

@author: raviteja
"""
from __future__ import with_statement
import getopt
import pandas as pd
import numpy as np
import os, sys
import string
import csv
import gc
import re
import os,sys
import pickle
import logging
from dateutil import parser
import ConfigParser
from datetime import datetime

from ayasdi.core.api import Api
from ayasdi.core.models import create_model, apply_model


try:
    from urllib.error import HTTPError  # Python 3
    from urllib.parse import urlencode
    from urllib.request import Request, urlopen
    strings = str
except ImportError:
    from urllib import urlencode  # Python 2
    from urllib2 import HTTPError, Request, urlopen
    strings = unicode

def main():

    ######### Logging  #########
    logger = logging.getLogger(__name__)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    config = ConfigParser.RawConfigParser()
    config.read('c_car.config')

    #Network inputs from config file
    filename = config.get('Networks', 'file_name')
    logger.info('Reading the data file in a dataframe')
    car_fn =  pd.read_table(
                '../../../data/ccar_data_5years.csv',
                sep=',',
                index_col=None,
                header=0,
                error_bad_lines=False,
                warn_bad_lines=True,
                na_values=['NO DATA', ' ', ''],
                dtype=unicode
                )


    metric = config.get('Networks', 'metric')
    logger.info('Metric chosen is %s' %metric)
    
    lens1 = config.get('Networks', 'Lens1')
    lens2 = config.get('Networks', 'Lens2')

    logger.info('Lens chosen are %s & %s' %(lens1,lens2))

    equalize = config.get('Networks', 'Equalize')
    logger.info('Equalize property is set to %s' %equalize)

    res = config.get('Networks', 'Res')
    logger.info('Resolution is set to %s' %res)

    gain = config.get('Networks', 'Gain')
    logger.info('Gain is set to %s' %gain)

    cutoff = config.get('Networks', 'Cut_off')
    logger.info('cutoff is set to %s' %cutoff)

    col_name = config.get('Networks', 'Col_name')
    logger.info('Column name for autogrouping is %s' %col_name)

    num_groups = 3
    #num_groups = config.get('Networks', 'Num_groups')
    logger.info('Number of groups to be picked from autogrouping are %s' %num_groups)

    logger.info('Running Pythomaton calls')
    connection = Api()

    #Source uploaded to core
    source = upload_source(filename, connection)

    #Creating column sets and colorings
    column_list = car_fn.columns
    column_list = column_list[1:]
    col_set, coloring = generate_columnsets(source, column_list)

    #Build autoanalysis
    suggestions = auto_analysis(source, col_set)

    #Building Auto networks based on suggestions
    build_auto_network(suggestions, source)

    #Creating custom network
    network = custom_networks(source, metric, lens1, lens2, col_set, res, gain, equalize)

    #Get autogrouping instance
    autogroups = auto_grouping(network, source, cutoff, col_name)

    #Dictionary of top groups and 3 variables from eacg group
    groups_var = {}
    groups_var = get_variables(network, autogroups, car_fn, num_groups)

    #Pickling the results
    with open('groups_var.pickle', 'wb') as handle:
        pickle.dump(groups_var, handle)


def create_connection():
    #API
    try:
        Connection = Api()    
        logger.info('API Connection is made')
    except ConnectionError as e:
        logger.info('Connection failed. Check Pythomaton documentation on how to connect to Pythomaton')

    return Connection


def upload_source(filename, connection):
    '''Verifies and uploads a csv file to Ayasdi core
    '''
    try:
        connection.delete_source(name=filename)
        logging.info('Deleted the source if it already exists')
    except:
        pass

    source = connection.upload_source(filename)
    logging.info('Uploaded the source')
    return source


def generate_columnsets(source, column_list):
	''' Pass the list of column numbers to select columns used to build network
	'''
	logging.info('Coloring based on rows per node')
	coloring = source.get_coloring(name="Rows per Node")

	logging.info('Creating a column set')
	col_set = source.create_column_set(column_list, "cs")

	return col_set, coloring


def auto_analysis(source, col_set):

    ''' Pass a column set and get autoanalyssis suggestions to build a network
    '''
    suggestions =  source.get_auto_analysis_suggestions(col_set)

    source.sync()

    logging.info("The source has %s networks." % len(source.get_networks()))
    logging.info("The suggestions are %s" %(suggestions))

    return suggestions


def build_auto_network(suggestions, source):

    ''' Build a network based on the suggestions provided(passed)
    '''
    running_jobs = []
    logging.info('Creating network based on suggestions')
    for ind, suggestion in enumerate(suggestions):
        network_name = 'Analysis %s' % ind
        print "Starting %s." % network_name
        new_job = source.create_network(network_name, suggestion)
        running_jobs.append(new_job)

    logging.info('Running jobs are %s' %(running_jobs))


def custom_networks(source, metric, lens1, lens2, col_set, res, gain, equalize):

    '''Custom network built on metrics and lens passed in config file.
    '''

    network = source.create_network("Custom_Network",{
        'metric': {'id': metric}, 
        'column_set_id': col_set['id'], 
        'lenses': [{'resolution': res, 'id': lens1, 
                    'equalize': equalize, 'gain': gain},
                   {'resolution': res, 'id': lens2, 
                    'equalize': equalize, 'gain': gain}] 
        } 
        )

    return network

def auto_grouping(network, source, cutoff, column_name):
    ''' Auto grouping based on community detection algorithm
    '''
    coloring = source.create_coloring(name='%s_coloring' %(column_name),
                                      column_name=column_name)
    coloring_values = network.get_coloring_values(name='%s_coloring' %(column_name))
    autogroups = network.autogroup(cutoff_strength=cutoff,
                               coloring_values=coloring_values)
    for ind, autogroup in enumerate(autogroups):
        network.create_node_group(name="Autogroup%s" % ind,
                               nodes=autogroup)
                                   
    return autogroups

def get_variables(network, autogroups, car_fn, num_groups):
    '''Getting top variables from top groups created by autogrouping
    '''
    logging.info('Sorting autogrouping based on the length of nodes in each group')
    autogroups.sort(key=len, reverse=True)
    logging.info('Picking top groups and sorting variables in each of them based on abs corrleation')
    top_groups = [[] for i in range(num_groups)]
    top_groups = autogroups[:num_groups]
    logging.info('Correlating points with actual records from dataframe to sort correlation')

    groups = [[] for i in range(num_groups)]
    car_fn.rename(columns = {'Unnamed: 0': 'var'}, inplace=True)
    var_cols = ['var','abscorr']

    logging.info('Capturing top groups and variables to a list of lists')
    logging.info('Sorting groups based on abs. correlation')
    for ind in range(len(top_groups)):
        groups[ind] = car_fn.loc[autogroups[ind]][var_cols].sort(['abscorr'], ascending=[False])

    logging.info('Pick top 3 variables from each group and put it in a dictionary')
    groups_dict = {}
    for val in range(len(groups)):
        groups_dict['group_'+str(val)] = groups[val]['var'][:3].values.tolist()

    return groups_dict

if __name__ == '__main__':
    main()