import ConfigParser

config = ConfigParser.RawConfigParser()

# When adding sections or items, add them in the reverse order of
# how you want them to be displayed in the actual file.
# In addition, please note that using RawConfigParser's and the raw
# mode of ConfigParser's respective set functions, you can assign
# non-string values to keys internally, but will receive an error
# when attempting to write to a file or when you get it in non-raw
# mode. SafeConfigParser does not allow such assignments to take place.


#Data Configurables
config.add_section('Data')

#Quandl token
config.set('Data', 'token', 'jRvWAzR5gCDPbKevFrEd')
#Add a file
config.set('Data', 'r_raw', '/Users/raviteja/Documents/workspace/c_car/data/Revenue_Forecast.txt')
#Start Date for timeseries
config.set('Data', 's_date', '2011-11-15')
#End Date for timeseries
config.set('Data', 'e_date', '2015-05-15')
#Variables index list
config.set('Data', 'variables', ["ODA/IND_NGDPD","FRED/GDP","YAHOO/INDEX_VIX.4","FRED/AUD3MTD156N","CHRIS/ASX_YS1","ODA/BRA_NGDPD","FRED/CHNCPIALLAINMEI","ODA/CHN_NGDP_RPCH","RATEINF/CPI_EUR","AUTONOMY/SWAP_05Y_EUR","ODA/IDN_NGDPD","FRED/JPY1MTD156N","ODA/KOR_NGDPD","FRED/DEXMXUS","ODA/RUS_NGDPD","ODA/TWN_NGDPD","FRED/TEDRATE","RATEINF/CPI_GBR","FED/RXI_US_N_B_UK","FRED/INTGSBGBM193N","ODA/GBR_NGDPD","UKONS/LMS_MGSX_M","UMICH/SOC1","OFDP/INDEX_MARKIT_CDXNAHY","OFDP/INDEX_MARKIT_CDXNAIG","OFDP/INDEX_CDXEM","RATEINF/CPI_USA","FRED/DFF","BUNDESBANK/BBK01_WT5511","FED/FL075035223_Q","FRED/DGS10","FED/RILSPDEPM01_N_B","FRED/DTB1YR","FED/RILSPDEPM03_N_B","FRED/DTB3","FRED/DSWP5","FED/RILSPDEPM06_N_B","DOE/RWTC","YAHOO/INDEX_GSPC","FRED/UNRATE","FED/FL073163013_Q","ML/BBBEY"])


#Network configurables
config.add_section('Networks')


#
config.set('Networks', 'file_name', '/Users/raviteja/Documents/workspace/c_car/data/ccar_data_5years.csv')
#Metrics options : 
config.set('Networks', 'Metric', 'Absolute Correlation')
#Lens options :
config.set('Networks', 'Lens1', 'Neighborhood Lens 1')
config.set('Networks', 'Lens2', 'Neighborhood Lens 2')
#Equalize options : True, False
config.set('Networks', 'Equalize', 'True')
#Resolution
config.set('Networks', 'Res', 30)
#Gain
config.set('Networks', 'Gain', 2.5000000000000004)
#Cutoff for Autogrouping
config.set('Networks', 'Cut_off', 0.8)
#Column_name or names for autogrouping
config.set('Networks', 'Col_name', 'abscorr')
#Number of groups to pick from autogrouping
config.set('Networks', 'Num_groups', 3)





#Network configurables
config.add_section('Regression')

#Stats to get from models
config.set('Regression', 'Stat_cols', ['predictivePower','varSignificance','multiCollinearity','serCorrelation','heteroscedasticity','normality','modelParsimony'])




# Writing our configuration file to 'example.cfg'
with open('c_car.config', 'wb') as configfile:
    config.write(configfile)
