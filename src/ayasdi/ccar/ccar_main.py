# -*- coding: utf-8 -*-
"""
Created on Wed Jan  7 22:05:23 2015

@author: raviteja
"""

from __future__ import with_statement
import getopt
import pandas as pd
import numpy as np
import os, sys
import string
import csv
import gc
import re
import os,sys
import logging
from dateutil import parser
import ConfigParser
from datetime import datetime

#import ccar_persistence
import ccar_etl
import ccar_pythomaton
import ccar_regress

try:
    from urllib.error import HTTPError  # Python 3
    from urllib.parse import urlencode
    from urllib.request import Request, urlopen
    strings = str
except ImportError:
    from urllib import urlencode  # Python 2
    from urllib2 import HTTPError, Request, urlopen
    strings = unicode

def main():

	'''Main code which runs the workflow of ccar data preparation
	'''

	# logging.info('Updating SQlite db with the data read from Quandl')
	# ccar_persistence.main()

	logging.info("Executing the config file to generate parameters for getting data from Quandl and passing the file to network")
	ccar_etl.main()

	logging.info("Running ccar_pythomaton script to build networks on processed data in ccar_etl module")
	ccar_pythomaton.main()

	logging.info('Running Regression model on top variables')
	ccar_regress.main()



if __name__ == '__main__':
    main()
