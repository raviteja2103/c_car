import pandas as pd
import numpy as np
import math
import datetime
import xlrd

def formatData(self,sheet_name,start_date,end_date):
	self_sheet = df.parse(sheet_name,skiprows=1,skip_footer = 1)
	Q_range = pd.date_range(start_date, end_date, freq='Q')
	self_sheet.index = Q_range
	self_sheet = self_sheet.resample('D', fill_method='ffill')
	self_final = self_sheet[self_sheet.index.day==15]
	return self_final 

df = pd.ExcelFile('/Users/mramachandran/Desktop/CCAR_Demo/2015-macro-scenario-tables.xlsx')
start_date = "2001-01-01"
end_date = "2017-12-31"
base_domestic = formatData(df,"Table1A",start_date,end_date)	
base_intl = formatData(df,"Table1B",start_date,end_date)
base_list = [base_domestic,base_intl]
base_final = pd.concat(base_list,axis=1)
base_final = base_final.drop('Date ', axis=1)
print base_final.T

