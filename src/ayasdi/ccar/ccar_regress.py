import pandas as pd
import numpy as np
import math
import datetime
import statsmodels.api as sm
import itertools
import logging

from statsmodels.stats.stattools import durbin_watson
from statsmodels.stats.diagnostic import het_breushpagan
from statsmodels.stats.diagnostic import acorr_breush_godfrey
from statsmodels.stats.outliers_influence import variance_inflation_factor
from statsmodels.stats.stattools import jarque_bera
from statsmodels.api import tsa
from sklearn.cross_validation import LeaveOneOut
from sklearn.cross_validation import KFold
from sklearn import metrics
from itertools import product, permutations,combinations,chain

from ccar_etl import *
from ccar_pythomaton import *

#TODO: 
#1)Extend combinations to all subsets
#2)Define passing/fail criteria for each test
#3)Output dashboard for each model with test values

class ModelResult(object):
	
	logging.info("Holds Result of 1 model")
	def __init__(self,df_X,df_X_Const,df_Y,Model,ModelFit):		
		self.df_X = df_X
		self.df_X_Const = df_X_Const
		self.df_Y = df_Y
		self.Model = Model
		self.ModelFit = ModelFit
	
	def getParams(self):
		return self.ModelFit.params.values

	def predictivePower(self):
		return self.ModelFit.rsquared

	def varSignificance(self):
		return self.ModelFit.pvalues.values

	def multiCollinearity(self):
		df_X_mat = pd.DataFrame.as_matrix(self.df_X)
		vif = [None]*len(self.df_X.columns)
		for i in range(0,df_X_mat.shape[1]):
			vif[i] = variance_inflation_factor(df_X_mat,i)
		return vif	
	
	def serCorrelation(self):
		return [durbin_watson(self.ModelFit.resid), acorr_breush_godfrey(self.ModelFit)[3]]

	def heteroscedasticity(self):
		return het_breushpagan(self.ModelFit.resid, self.ModelFit.model.exog)[1]

	def normality(self):
		return jarque_bera(self.ModelFit.resid)[0]

	def modelParsimony(self):
		return [self.ModelFit.aic, self.ModelFit.bic]

def testStationarity(input_features):
	feature_df = reg_full[input_features]
	result = feature_df.apply(lambda x: tsa.adfuller(x,regression='nc'),axis = 0)
	result_pval = result[1]
	return result_pval

def buildModel(input_features,row_indices):	
	feature_df = reg_full[input_features].iloc[row_indices]
	feature_df_const = sm.add_constant(feature_df)
	output_df = reg_output.iloc[row_indices]
	reg_model = sm.OLS(output_df,feature_df_const,missing = 'drop')
	reg_model_fit = reg_model.fit()
	return ModelResult(feature_df,feature_df_const,output_df,reg_model,reg_model_fit)

def stabilityCoeff(input_features,row_indices):
	"""Cross_Validation: Coefficients percent changes were small: Training vs. Whole Model"""
	df = reg_full[input_features]
	kf = KFold(len(df))	
	param_list = []	
	for train_index,test_index in kf:
		train_object = buildModel(input_features, train_index)
		train_model_fit = train_object.ModelFit		
		param_list.append(train_model_fit.params.values)	
	param_matrix = np.vstack(param_list)
	full_model = buildModel(input_features,row_indices)
	full_model_param = full_model.ModelFit.params.values
	ratio_change = abs(np.divide(param_matrix,full_model_param))
	avg_pct_change = np.mean(np.mean(ratio_change,axis=0))
	return avg_pct_change

def stabilityPred(input_features):
	"""Cross_Validation: RMSE of training vs. RMSE of testing"""
	df = reg_full[input_features]
	kf = KFold(len(df))
	rmse_train_list = []
	rmse_test_list = []
	for train_index, test_index in kf:		
		train_object = buildModel(input_features, train_index)
		train_model = train_object.ModelFit
		train_feature = train_object.df_X_Const
		train_output = train_object.df_Y
		train_predict = train_model.predict(train_feature)

		test_object = buildModel(input_features, test_index)
		test_model = test_object.ModelFit
		test_feature = test_object.df_X_Const
		test_output = test_object.df_Y
		test_predict = test_model.predict(test_feature)

		rmse_train_list.append(np.sqrt(metrics.mean_squared_error(train_output,train_predict)))
		rmse_test_list.append(np.sqrt(metrics.mean_squared_error(test_output,test_predict)))
	return np.mean(np.divide(rmse_train_list,rmse_test_list))	

def sensitivityTest(input_features, row_indices):
	"""RMSE Ratio of sensitized coefficients vs. regular coefficients"""
	predictor_object = buildModel(input_features,row_indices)
	predictor_model_fit = predictor_object.ModelFit
	full_output = predictor_object.df_Y
	low_params = 0.9*(predictor_model_fit.params.values)
	high_params = 1.1*(predictor_model_fit.params.values)

	predictor_model = predictor_object.Model
	predictor_feature = predictor_object.df_X_Const
	predictor_vals_low = predictor_model.predict(low_params,predictor_feature)
	predictor_vals_high = predictor_model.predict(high_params,predictor_feature)

	rmse_low = np.sqrt(metrics.mean_squared_error(full_output,predictor_vals_low))
	rmse_high = np.sqrt(metrics.mean_squared_error(full_output,predictor_vals_high))
	return np.divide(rmse_low, rmse_high)

def sensitivityTestLOO(input_features):
	"""LeaveOneOut: Highlight Outlier Points"""
	df = reg_full[input_features]
	loo = LeaveOneOut(len(df))
	model_collection = []
	for train_index, test_index in loo:
		result = buildModel(input_features, train_index)
	return result

def getDashBoard(models_comb,models,num_groups, feature_comb_list):
	"""Building a dataframe to store all the stats from the models
	"""
	logging.info("Dataframe indices")
	index_list = []
	for ind_list in range(len(models_comb)):
		index_list.append('model_'+ '%s'%ind_list)

	logging.info("Updating Dataframe indices")
	models['model_no'] = np.NaN
	models['model_no'] = index_list
	models.set_index('model_no')

	logging.info("Updating variables in each columns")

	models['variable'] = np.NaN
	for ind in range(len(models_comb)):
		models['variable'][ind] = feature_comb_list[ind]

	logging.info("Repositioning the columns")

	for col in range(len(models_comb)):

			models['predictivePower'][col] = models_comb[col].predictivePower()
			models['modelObject'][col] = models_comb[col].getParams()
			models['varSignificance'][col] = models_comb[col].varSignificance()
			models['multiCollinearity'][col] = models_comb[col].multiCollinearity()
			models['serCorrelation'][col] = models_comb[col].serCorrelation()
			models['heteroscedasticity'][col] = models_comb[col].heteroscedasticity()
			models['normality'][col] = models_comb[col].normality()
			models['modelParsimony'][col] = models_comb[col].modelParsimony()

	subset = models['variable']
	tuples = [tuple(x) for x in subset.values]

	models['var-coeff'] = dict(zip(tuples, models.modelObject)).items()

	models = models[['model_no','var-coeff','predictivePower',
				 'varSignificance',
				 'multiCollinearity',
				 'serCorrelation',
				 'heteroscedasticity',
				 'normality',
				 'modelParsimony']]

	return models

	
def main() :

	config = ConfigParser.RawConfigParser()
	config.read('c_car.config')

	#stat_cols  = config.get('Regression', 'Stat_cols')
	stat_cols  = ['modelObject','predictivePower', 'varSignificance', 'multiCollinearity', 'serCorrelation', 'heteroscedasticity', 'normality', 'modelParsimony']
	num_groups = 3
	#num_groups = config.get('Networks', 'Num_groups')
	#"""Sample Input Data (Output from AutoGrouping)"""
	# groups_dict = {}
	# groups_dict["Group1"] = ['YAHOO.INDEX_VIX - Close','YAHOO.INDEX_VIX - Close_log']
	# groups_dict["Group2"] = ['FRED.GDP - Value','FRED.GDP - Value_log']
	# groups_dict["Group3"] = ['ODA.IND_NGDPD - Value']

	logging.info('Reading pickled iris_ready data')
	with open('iris_ready.pickle', 'rb') as handle:
		iris_ready = pickle.load(handle)

	logging.info('Reading pickled groups_var data')
	with open('groups_var.pickle', 'rb') as handle:
		groups_var = pickle.load(handle)
	
	"""Pre-Processing"""
	reg_full = iris_ready.T
	reg_full['Revenue'] = rev_clean
	reg_full = reg_full.dropna()
	reg_data = [col for col in reg_full.columns if col not in ['Revenue']]
	reg_output = reg_full['Revenue']  
	"""Single Model Tests"""
	#feature_list = ['YAHOO.INDEX_VIX - Close','FRED.GDP - Value']
	feature_list = []
	for i in range(len(groups_var)):
	    feature_list.append(groups_var['group_%s'%i][0])

	full_row_list = range(len(reg_output))
	test_model = buildModel(feature_list,full_row_list)
	test_rsquared = test_model.predictivePower()
	test_sensitivityTest = stabilityCoeff(feature_list,full_row_list)

	logging.info("Extend to all Combination of Models")
	feature_comb = [val for val in product(*groups_var.values())]
	feature_comb_list = [list(elem) for elem in feature_comb]
	models_comb = []
	for i in feature_comb_list:
		models_comb.append(buildModel(i,full_row_list))

	logging.info("Building a dash using models_comb")
	models = pd.DataFrame(columns=stat_cols)
	models = getDashBoard(models_comb,models,num_groups, feature_comb_list)
		
	with open('models.pickle', 'wb') as handle:
		pickle.dump(models, handle)

if __name__ == "__main__":
	main()